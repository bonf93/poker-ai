import Vue from 'vue'
import Vuex from 'vuex'
import game from './game'
// import createPersistedState from 'vuex-persistedstate'

// import deck from './deck'
Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    game
    // deck
  }/* ,
  plugins: [createPersistedState()] */
})
