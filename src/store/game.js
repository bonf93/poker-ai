import Vue from 'vue'
import utils from '@/utils'

const state = {
  jackpot: 25000,
  pot: 0,
  status: 1, // 0 apertura, 1 cambio carte, 2 puntate
  apertura: null, // 0 cpu  1 player
  cpu_credits: 25000,
  player_credits: 25000,
  cpu_pot: 0,
  player_pot: 0,
  cpu_sidepot: 0,
  player_sidepot: 0,
  cpu_change: null,
  player_change: null,
  dealer: 0, // 0=cpu, 1=player
  turn: 1, // 0 cpu   1 player
  cpu_cards: [],
  player_cards: [],
  deck: []
}

const actions = {

  ia_apertura ({state, commit, dispatch}, isFirstTime) {
    console.log('cpu decide se può aprore')
    setTimeout(function () {
      let a = utils.ia.apertura(state)
      if (a) {
        console.log('la cpu apre di ' + a)
        commit('ia_apertura', a)
        commit('toggle_turn')
      } else
        if (state.dealer === 0) {
          console.log('La cpu non apre. Il player non aveva aperto. Fine della mano')
          dispatch('end_hand')
        } else {
          console.log('La cpu non apre. Cambio turno')
          commit('toggle_turn')
        }
    }, 2000)
  },

  ia_decide ({state, commit, dispatch}) {
    setTimeout(function () {
      let a = utils.ia.decide(state)
      console.log('ia decided ' + a)
      if (!a && state.cpu_pot < state.player_pot)
        dispatch('ia_fold')
      else if (!a && state.cpu_pot === state.player_pot)
        dispatch('ia_check')
      else if (a && (state.cpu_pot + a) === state.player_pot)
        dispatch('ia_vedi')
      else if (a && (state.cpu_pot + a) > state.player_pot)
        dispatch('ia_puntarilancia', a)
    }, 2000)
  },

  ia_fold ({state, commit, dispatch}) {
    dispatch('end_hand', 1)
  },
  ia_check ({state, commit, dispatch}) {
    commit('toggle_turn')
  },
  ia_vedi ({state, commit, dispatch}) {
    let d = state.player_pot - state.cpu_pot
    state.cpu_credits -= d
    state.cpu_pot += d
    state.pot += d
    if (state.status === 0) {
      commit('set_status', 1)
      commit('reset_turn')
      if (state.dealer === 1)
        dispatch('ia_change')
    }
  },
  ia_puntarilancia ({state, commit, dispatch}, a) {
    state.cpu_credits -= a
    state.cpu_pot += a
    state.pot += a
    commit('toggle_turn')
  },

  ia_change ({state, commit, dispatch}) {
    let c = utils.ia.changeCards(state)
    state.cpu_change = c.length
    for (let i = 0; i < c.length; i++)
      Vue.set(state.cpu_cards, c[i], state.deck.pop())
    if (state.dealer === 0) {
      commit('set_status', 2)
      commit('set_turn', parseInt(state.apertura))
    } else commit('toggle_turn')
  },

  pl_apertura ({state, commit, dispatch}, a) {
    console.log('pl a')
    if (a) {
      commit('pl_apertura', a)
      commit('toggle_turn')
      dispatch('ia_decide')
    } else
      if (state.dealer === 1)
        dispatch('end_hand')
      else {
        commit('toggle_turn')
        dispatch('ia_apertura')
      }
  },

  pl_fold ({state, commit, dispatch}) {
    dispatch('end_hand', 0)
  },
  pl_check ({state, commit, dispatch}) {
    commit('toggle_turn')
  },
  pl_vedi ({state, commit, dispatch}) {
    let d = state.cpu_pot - state.player_pot
    state.player_pot += d
    state.pot += d
    if (state.status === 0) {
      commit('set_status', 1)
      commit('reset_turn')
      if (state.dealer === 1)
        dispatch('ia_change')
    }
  },
  pl_puntarilancia ({state, commit, dispatch}, a) {
    state.player_credits -= a
    state.player_pot += a
    state.pot += a
    commit('toggle_turn')
    dispatch('ia_decide')
  },
  pl_change ({state, commit, dispatch}) {
    commit('pl_change')
    if (state.dealer === 1) {
      commit('set_status', 2)
      commit('set_turn', parseInt(state.apertura))
    } else {
      commit('toggle_turn')
      dispatch('ia_change')
    }
  },

  end_hand ({state, commit, dispatch}, winner) {
    if (winner === 1)
      state.player_credits += state.pot
    else if (winner === 0)
      state.cpu_credits += state.pot
    else {
      state.cpu_credits += state.cpu_pot
      state.player_credits += state.player_pot
    }
    dispatch('start_hand')
  },
  start_hand ({state, commit, dispatch}) {
    state.cpu_pot = 0
    state.player_pot = 0
    state.pot = 0
    state.cpu_cards = []
    state.player_cards = []
    state.apertura = null
    commit('set_status', 0)
    commit('init_deck')
    commit('shuffle')
    commit('spread')
    commit('toggle_dealer')
    if (state.dealer === 1)
      dispatch('ia_apertura', true)
  }
}

const mutations = {
  ia_apertura (state, a) {
    if (a) {
      state.cpu_pot += a
      state.cpu_credits -= a
      state.pot += a
      if (state.apertura === null)
        state.apertura = 0
    }
  },
  pl_apertura (state, a) {
    if (a) {
      state.player_pot += a
      state.player_credits -= a
      state.pot += a
      if (state.apertura === null)
        state.apertura = 1
    }
  },
  toggle_dealer (state) {
    state.dealer ^= 1
    state.turn = state.dealer ^ 1
  },
  toggle_turn (state) {
    state.turn ^= 1
  },
  reset_turn (state) {
    state.turn = state.dealer ^ 1
  },
  set_turn (state, t) {
    state.turn = t
  },
  set_status (state, s) {
    state.status = s
  },
  spread (state) {
    for (let i = 0; i < 5; i++) {
      state.cpu_cards.push(state.deck.pop())
      state.player_cards.push(state.deck.pop())
    }
  },

  init_deck: function (state) {
    state.deck = []

    let vals = [8, 9, 10, 11, 12, 13, 14]
    let suits = ['Hearts', 'Diamonds', 'Spades', 'Clubs']

    for (let i = 0; i < vals.length; i++)
      for (let j = 0; j < suits.length; j++)
        state.deck.push({ id: `${i}${j}`, val: vals[i], suit: suits[j], selected: false })
  },

  pl_change: function (state) {
    let toChange = state.player_cards.filter((c, i) => {
      if (c.selected)
        c.idx = i
      return c.selected
    })
    state.player_change = toChange.length
    for (let i = 0; i < toChange.length; i++)
      Vue.set(state.player_cards, toChange[i].idx, state.deck.pop())
  },

  toggleChange: function (state, c) {
    c.selected = !c.selected
  },

  shuffle: function (state) {
    var a = state.deck
    for (let i = a.length; i; i--) {
      let j = Math.floor(Math.random() * i)
      let t = a[i - 1]
      Vue.set(a, i - 1, a[j])
      Vue.set(a, j, t)
    }
  }
}

const getters = {
  game_status (state) {
    return state.status
  },
  player_cards (state) {
    return state.player_cards
  },
  player_pot (state) {
    return state.player_pot
  },
  player_credits (state) {
    return state.player_credits
  },
  cpu_pot (state) {
    return state.cpu_pot
  },
  cpu_credits (state) {
    return state.cpu_credits
  },
  cpu_cards (state) {
    return state.cpu_cards
  },
  pot (state) {
    return state.pot
  },
  dealer () {
    return state.dealer
  },
  apertura () {
    return state.apertura
  },
  turn () {
    return state.turn
  },
  deck: function (state) {
    return state.deck
  },
  state: function (state) {
    return state
  }
}

export default {
  // namespaced: true,
  state,
  actions,
  mutations,
  getters
}
