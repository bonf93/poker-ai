import Vue from 'vue'

const state = {
  deck: []
}

const mutations = {
  init_deck: function (state) {
    state.deck = []

    let vals = [8, 9, 10, 11, 12, 13, 14]
    let suits = ['Hearts', 'Diamonds', 'Spades', 'Clubs']

    for (let i = 0; i < vals.length; i++)
      for (let j = 0; j < suits.length; j++)
        state.deck.push({ id: `${i}${j}`, val: vals[i], suit: suits[j] })
  },

  shuffle: function (state) {
    var a = state.deck
    for (let i = a.length; i; i--) {
      let j = Math.floor(Math.random() * i)
      let t = a[i - 1]
      Vue.set(a, i - 1, a[j])
      Vue.set(a, j, t)
    }
  }
}

const getters = {
  deck: function (state) {
    return state.deck
  }
}

const actions = {

}

export default {
  state,
  mutations,
  actions,
  getters
}
