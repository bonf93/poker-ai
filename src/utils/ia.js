import score from './score'
import prob from './prob'
var apertura = function (state) {
  if (!score.apertura(state.cpu_cards)) // se nessuno ha già aperto e non posso aprire, non apro
    return false
  if (prob.oneOf(1, 100)) {
    console.log('1/100 non apro')
    return false
  }

  if (prob.oneOf(1, 15)) {
    console.log('1/15 apro a caso circa 15/100 dei miei crediti')
    return prob.normal(Math.floor(state.cpu_credits / 100 * 15))
  }
  return getApertura(state)
}

var decide = function (state) {
  if (state.status === 0)
    if (state.apertura === 0) { /* ha aperto il pc, il player ha rilanciato */
      let d = Math.abs(state.player_pot - state.cpu_pot)
      console.log('player rilancia') // continua ad qui e gestisci in maniera piu simile al caso state.apertura= 1
      let s = score.getScore(state.cpu_cards)
      if (d >= Math.floor(state.cpu_credits / 3)) {
        console.log('piu di 1/3')
        if (s === 'poker' || s === 'scala reale' || s === 'colore')
          return state.cpu_credits
        else if (s === 'full')
          return state.player_pot - state.cpu_pot
        else
          return false
      } else {
        console.log('meno di 1/3')
        if (s === 'poker' || s === 'scala reale')
          return state.cpu_credits
        else if (s === 'full' || s === 'colore')
          return d
        else if ((s === 'tris' || s === 'scala') && d <= Math.floor(state.cpu_credits / 7))
          return d
        else if (s === 'doppia coppia' && score.getDoppiaCoppiaVal(state.cpu_cards) >= 11 && d <= Math.floor(state.cpu_credits / 8))
          return d
        else if (s === 'coppia' && score.getCoppiaVal(state.cpu_cards) >= 13 && d <= Math.floor(state.cpu_credits / 9))
          return d
        else
          return false
      }
    } else if (state.apertura === 1) {
      let a = getApertura(state)
      let s = score.getScore(state.cpu_cards)
      let d = Math.abs(state.player_pot - a)
      if (a === state.player_pot)
        return a
      if (a > state.player_pot) {
        if (d <= Math.floor(state.cpu_credits / 15))
          return parseInt(state.player_pot)
        return a
      }
      if (a < state.player_pot) {
        if (d < Math.floor(state.cpu_credits / 15))
          return parseInt(state.player_pot)
        if (s === 'poker' || s === 'full' || s === 'scala reale' || s === 'colore')
          return parseInt(state.player_pot)
        if ((s === 'tris' || s === 'scala') && d < Math.floor(state.cpu_credits / 9))
          return parseInt(state.player_pot)
        if (s === 'doppia coppia' && score.getDoppiaCoppiaVal(state.cpu_cards) >= 13 && d < Math.floor(state.cpu_credits / 11))
          return parseInt(state.player_pot)
        if (s === 'coppia' && score.getCoppiaVal(state.cpu_cards) >= 13 && d < Math.floor(state.cpu_credits / 13))
          return parseInt(state.player_pot)
      }
      return false
    }
}

var getApertura = function (state) {
  let s = score.getScore(state.cpu_cards)
  switch (s) {
    case 'coppia':
      if (score.is45colore(state.cpu_cards))
        return prob.normal(Math.floor(state.cpu_credits / 100 * 5))
      let coppiaVal = score.getCoppiaVal(state.cpu_cards)
      console.log('cpval', coppiaVal)
      if (coppiaVal >= 13)
        return prob.normal(Math.floor(state.cpu_credits / 100 * 7))

      return prob.normal(Math.floor(state.cpu_credits / 100 * 3))
    case 'doppia coppia':
      let dpval = score.getDoppiaCoppiaVal(state.cpu_cards)
      console.log('dpval', dpval)
      if (dpval > 12)
        return prob.normal(Math.floor(state.cpu_credits / 100 * 15))
      else if (dpval > 10)
        return prob.normal(Math.floor(state.cpu_credits / 100 * 12))
      else
        return prob.normal(Math.floor(state.cpu_credits / 100 * 10))
    case 'tris':
      let tval = score.getTrisVal(state.cpu_cards)
      console.log('tval', tval)
      if (tval > 12)
        return prob.normal(Math.floor(state.cpu_credits / 100 * 18))
      else if (tval > 10)
        return prob.normal(Math.floor(state.cpu_credits / 100 * 16))
      else
        return prob.normal(Math.floor(state.cpu_credits / 100 * 15))
    case 'scala':
      let sval = score.getScalaVal(state.cpu_cards)
      console.log('sval', sval)
      if (sval > 12)
        return prob.normal(Math.floor(state.cpu_credits / 100 * 18))
      else if (sval > 10)
        return prob.normal(Math.floor(state.cpu_credits / 100 * 16))
      else
        return prob.normal(Math.floor(state.cpu_credits / 100 * 15))
    case 'full':
      let fval = score.getFullVal(state.cpu_cards)
      console.log('fval', fval)
      if (fval > 12)
        return prob.normal(Math.floor(state.cpu_credits / 100 * 20))
      else if (fval > 10)
        return prob.normal(Math.floor(state.cpu_credits / 100 * 18))
      else
        return prob.normal(Math.floor(state.cpu_credits / 100 * 16))
    case 'colore':
      return prob.normal(Math.floor(state.cpu_credits / 100 * 21))
    case 'poker':
      return prob.normal(Math.floor(state.cpu_credits / 100 * 22))
    case 'scala reale':
      return prob.normal(Math.floor(state.cpu_credits / 100 * 23))
  }
}

var changeCards = function (state) {
  // volendo, 1/15 servito a caso. oppure salvare sullo store l'1/15 di apertura a caso e continuare in quel caso a bluffare. magari in maniera probabilistica anche qui
  var c = state.cpu_cards
  var s = score.getScore(c)
  if (state.dealer === 1) {
    if (s === 'scala reale' || s === 'colore' || s === 'full' || s === 'scala')
      return []
    if (s === 'poker')
      return prob.oneOf(1, 3) ? [] : score.getPokerChange(c)
    if (s === 'tris')
      return prob.oneOf(1, 3) ? [score.getTrisChange(c)[0]] : score.getTrisChange(c)
    if (s === 'doppia coppia')
      return prob.oneOf(1, 10) ? [] : score.getDoppiaCoppiaChange(c) // piccolo ulteriore bluff sulla doppia coppia
    if (score.is45colore(c)) // controllo i 4/5 di colore PRIMA di coppia e carta alta. Inserire qui in seguito anche i 4/5 di scala
      return score.get45coloreChange(c)
    if (s === 'coppia') {
      // let v = score.getCoppiaVal(c) gestire
      let sc = score.getCoppiaChange(c)
      if (sc[0].val >= 13)
        return prob.oneOf(1, 3) ? [c.indexOf(sc[1]), c.indexOf(sc[2])] : [c.indexOf(sc[0]), c.indexOf(sc[1]), c.indexOf(sc[2])]
      else
        return prob.oneOf(1, 6) ? [c.indexOf(sc[1]), c.indexOf(sc[2])] : [c.indexOf(sc[0]), c.indexOf(sc[1]), c.indexOf(sc[2])]
    }
    if (s === 'carta alta')
      return [2, 3] // gestire
  } else { // attualmente l'else è identico. va differenziato nel caso in cui il player si proclami servito. a quel punto, se sta dicendo la verità, giocare per qualcosa di più basso della scala non avrebbe senso
    if (s === 'scala reale' || s === 'colore' || s === 'full' || s === 'scala')
      return []
    if (s === 'poker')
      return prob.oneOf(1, 3) ? [] : score.getPokerChange(c)
    if (s === 'tris')
      return prob.oneOf(1, 3) ? [score.getTrisChange(c)[0]] : score.getTrisChange(c)
    if (s === 'doppia coppia')
      return prob.oneOf(1, 10) ? [] : score.getDoppiaCoppiaChange(c) // piccolo ulteriore bluff sulla doppia coppia
    if (score.is45colore(c)) // controllo i 4/5 di colore PRIMA di coppia e carta alta. Inserire qui in seguito anche i 4/5 di scala
      return score.get45coloreChange(c)
    if (s === 'coppia') {
      // let v = score.getCoppiaVal(c) gestire
      let sc = score.getCoppiaChange(c)
      if (sc[0].val >= 13)
        return prob.oneOf(1, 3) ? [c.indexOf(sc[1]), c.indexOf(sc[2])] : [c.indexOf(sc[0]), c.indexOf(sc[1]), c.indexOf(sc[2])]
      else
        return prob.oneOf(1, 6) ? [c.indexOf(sc[1]), c.indexOf(sc[2])] : [c.indexOf(sc[0]), c.indexOf(sc[1]), c.indexOf(sc[2])]
    }
    if (s === 'carta alta')
      return [2, 3] // gestire
  }
}
export default {
  apertura,
  decide,
  changeCards
}
