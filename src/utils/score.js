var isScalaReale = function (c) {
  return isScala(c) && isColore(c)
}

var isScala = function (c) {
  var sorted = c.concat().sort((a, b) => a.val - b.val)
  var ret = isConsecutive(sorted)
  if (ret) return ret

  if (sorted[sorted.length - 1].val === 14) {
    console.log('ce un asso', sorted.concat().splice(0, sorted.length - 1))
    let t = isConsecutive(sorted.concat().splice(0, sorted.length - 1))
    if (t && sorted[0].val === 8)
      ret = true
  }

  return ret
}

var isConsecutive = function (c) {
  for (let i = 0; i < c.length - 1; i++)
    if (c[i].val !== c[i + 1].val - 1)
      return false

  return true
}

var getScalaVal = function (c) {
  let a = c.reduce((prev, cur) => prev.val < cur.val ? prev.val : cur.val)
  if (a === 14)
    for (let i = 0; i < c.length; i++)
      if (c[i].val === 8)
        return 11

  return a
}

var isColore = function (c) {
  let suit = c[0].suit
  for (let i = 1; i < c.length; i++)
    if (c[i].suit !== suit)
      return false

  return true
}

var is45colore = function (c) {
  let h, d, s, cl
  h = d = s = cl = 0
  for (let i = 0; i < c.length; i++)
    switch (c[i].suit) {
      case 'Hearts':
        h++
        break
      case 'Diamonds':
        d++
        break
      case 'Spades':
        s++
        break
      case 'Clubs':
        cl++
    }

  if (h === 4 || d === 4 || s === 4 || cl === 4)
    return true
  return false
}

var get45coloreChange = function (c) {
  for (let i = 0; i < c.length; i++) {
    let tot = 0
    for (let k = 0; k < c.length; k++) {
      if (i === k)
        continue
      if (c[i].suit !== c[k].suit)
        tot++
      if (tot >= 2)
        return [i]
    }
  }
}

var isPoker = function (c) {
  for (let i = 0; i < 2; i++) {
    let val = c[i].val
    let tot = 1
    for (let k = 0; k < c.length; k++) {
      if (i === k) continue
      if (val === c[k].val)
        tot++
    }
    if (tot === 4)
      return true
  }

  return false
}

var isFull = function (c) {
  let v1 = c[0].val
  let v2 = null
  let t1 = 0
  let t2 = 0
  for (let i = 1; i < c.length; i++)
    if (c[i].val !== v1) {
      v2 = c[i].val
      break
    }

  for (let i = 0; i < c.length; i++)
    if (c[i].val === v1)
      t1++
    else if (c[i].val === v2)
      t2++

  if ((t1 === 2 && t2 === 3) || (t1 === 3 && t2 === 2))
    return true

  return false
}

var getFullVal = function (c) {
  let v1 = c[0].val
  let v2 = null
  let t1 = 0
  let t2 = 0

  for (let i = 1; i < c.length; i++)
    if (c[i].val !== v1) {
      v2 = c[i].val
      break
    }

  for (let i = 0; i < c.length; i++)
    if (c[i].val === v1)
      t1++
    else if (c[i].val === v2)
      t2++

  if (t1 === 3)
    return v1
  else if (t2 === 3)
    return v2

  throw new Error('getFullVal need a full point')
}

var isTris = function (c) {
  for (let i = 0; i < c.length; i++) {
    let v = c[i].val
    let t = 0
    for (let k = 0; k < c.length; k++)
      if (c[k].val === v)
        t++

    if (t === 3)
      return true
  }

  return false
}

var getTrisVal = function (c) {
  for (let i = 0; i < c.length; i++) {
    let v = c[i].val
    let t = 0
    for (let k = 0; k < c.length; k++)
      if (c[k].val === v)
        t++

    if (t === 3)
      return v
  }
}

var isDoppiaCoppia = function (c) {
  let o = {}
  for (let i = 0; i < c.length; i++)
    if (!o[c[i].val]) o[c[i].val] = 1
    else o[c[i].val]++

  let t = 0
  for (let key in o) {
    if (o[key] === 2) t++
    if (t === 2) return true
  }
  return false
  /*
  for (let i = 0; i < c.length - 1; i++) {
    let v1 = c[i].val
    let v2 = c[i + 1].val
    let t1 = 0
    let t2 = 0
    for (let k = 0; k < c.length; k++)
      if (c[k].val === v1)
        t1++
      else if (c[k].val === v2)
        t2++

    if (t1 === 2 && t2 === 2)
      return true
  }

  return false
  */
}

var getDoppiaCoppiaVal = function (c) {
  let o = []
  for (let i = 0; i < c.length; i++)
    for (let k = 0; k < c.length; k++)
      if (i !== k && c[i].val === c[k].val && !o.includes(c[i].val))
        o.push(c[i].val)

  return Math.max(o[0], o[1])
}

var isCoppia = function (c) {
  for (let i = 0; i < c.length; i++) {
    let v = c[i].val
    let t = 0
    for (let k = 0; k < c.length; k++)
      if (c[k].val === v)
        t++

    if (t === 2)
      return true
  }

  return false
}

var getCoppiaVal = function (c) {
  for (let i = 0; i < c.length; i++) {
    let n = c[i].val
    let t = 0
    for (let k = 0; k < c.length; k++)
      if (c[k].val === n)
        t++
    if (t === 2)
      return n
  }
}

var apertura = function (c) {
  let s = getScore(c)
  if (scores.indexOf(s) > 1)
    return true
  if (scores.indexOf(s) === 1)
    for (let i = 0; i < c.length; i++) {
      let n = c[i].val
      let t = 0
      for (let k = 0; k < c.length; k++)
        if (c[k].val === n)
          t++
      if (t === 2 && n >= 11)
        return true
    }
  return false
}

/* c is an array of objects object that contains a score property and a cards property */
// const c = [
//   [{val: 8, suit: 'Hearts'},
//   {val: 8, suit: 'Diamonds'},
//   {val: 9, suit: 'Spades'},
//   {val: 10, suit: 'Clubs'},
//   {val: 11, suit: 'Hearts'}
//   ],
//   [{val: 8, suit: 'Clubs'},
//   {val: 8, suit: 'Spades'},
//   {val: 9, suit: 'Clubs'},
//   {val: 10, suit: 'Diamonds'},
//   {val: 11, suit: 'Clubs'}
//   ]
// ]

var getBestCoppia = function (c) {
  let c0Val = getCoppiaVal(c[0])
  console.log(c0Val)
  let c1Val = getCoppiaVal(c[1])
  console.log(c1Val)
  if (c0Val !== c1Val)
    if (c0Val > c1Val)
      return 0
    else
      return 1
  let scarto0 = c[0].filter((a) => a.val !== c0Val).sort((a, b) => b.val - a.val)
  let scarto1 = c[1].filter((a) => a.val !== c1Val).sort((a, b) => b.val - a.val)

  console.log(scarto0, scarto1)

  if (scarto0[0].val !== scarto1[0].val)
    if (scarto0[0].val > scarto1[0].val)
      return 0
    else
      return 1

  if (scarto0[1].val !== scarto1[1].val)
    if (scarto0[1].val > scarto1[1].val)
      return 0
    else
      return 1

  if (scarto0[2].val !== scarto1[2].val)
    if (scarto0[2].val > scarto1[2].val)
      return 0
    else
      return 1

  if (suits.indexOf(scarto0[0].suit) > suits.indexOf(scarto1[0].suit))
    return 0
  else
    return 1
}
var getBestDoppiaCoppia = function (c) {
  console.log('doppia coppia')
  return 1
}
var getBestTris = function (c) {
  console.log('tris')
  return 1
}
var getBestScala = function (c) {
  console.log('scala')
  return 1
}
var getBestFull = function (c) {
  console.log('full')
  return 1
}
var getBestColore = function (c) {
  console.log('colore')
  return 1
}
var getBestPoker = function (c) {
  console.log('poker')
  return 1
}

var getPokerChange = function (c) {
  for (let i = 0; i < c.length; i++) {
    let tot = 0
    for (let k = 0; k < c.length; k++) {
      if (i === k)
        continue
      if (c[i].val !== c[k].val)
        tot++
      if (tot >= 2)
        return [i]
    }
  }
}

var getTrisChange = function (c, t) {
  let a = []
  for (let i = 0; i < c.length; i++) {
    let tot = 0
    for (let k = 0; k < c.length; k++) {
      if (i === k)
        continue
      if (c[i].val !== c[k].val)
        tot++
      if (tot >= 3 && !a.includes(i))
        a.push(i)
    }
  }
  return a
}

var getDoppiaCoppiaChange = function (c) {
  for (let i = 0; i < c.length; i++) {
    let tot = 0
    for (let k = 0; k < c.length; k++) {
      if (i === k)
        continue
      if (c[i].val !== c[k].val)
        tot++
      if (tot >= 4)
        return [i]
    }
  }
}

var getCoppiaChange = function (c) {
  for (let i = 0; i < c.length; i++)
    for (let k = 0; k < c.length; k++)
      if (i !== k && c[i].val === c[k].val)
        return c.filter(e => e !== c[i] && e !== c[k]).sort((a, b) => b.val - a.val)
}

var getBestScalaReale = function (c) {
  console.log('scala reale')
  return 1
}
var getBestScore = function (c) {
  // c1 carte pc e c2 carte player
  let c0 = c[0]
  let c1 = c[1]
  let c0Score = getScore(c0)
  let c1Score = getScore(c1)
  if (c0Score !== c1Score)
    if (scores.indexOf(c0Score) > scores.indexOf(c1Score))
      return 0
    else
      return 1

  if (c0Score === 'coppia')
    return getBestCoppia(c)

  if (c0Score === 'doppia coppia')
    return c.indexOf(getBestDoppiaCoppia(c))

  if (c0Score === 'tris')
    return c.indexOf(getBestTris(c))

  if (c0Score === 'scala')
    return c.indexOf(getBestScala(c))

  if (c0Score === 'full')
    return c.indexOf(getBestFull(c))

  if (c0Score === 'colore')
    return c.indexOf(getBestColore(c))

  if (c0Score === 'poker')
    return c.indexOf(getBestPoker(c))

  if (c0.score === 'scala reale')
    return c.indexOf(getBestScalaReale(c))
}

// setTimeout(function () {
//   console.log(getBestScore(c))
// }, 1000)
var scores = [
  'carta alta',
  'coppia',
  'doppia coppia',
  'tris',
  'scala',
  'full',
  'colore',
  'poker',
  'scala reale'
]
let suits = ['Spades', 'Clubs', 'Diamonds', 'Hearts']

var getScore = function (c) {
  if (isScalaReale(c))
    return scores[8]
  if (isPoker(c))
    return scores[7]
  if (isColore(c))
    return scores[6]
  if (isFull(c))
    return scores[5]
  if (isScala(c))
    return scores[4]
  if (isTris(c))
    return scores[3]
  if (isDoppiaCoppia(c))
    return scores[2]
  if (isCoppia(c))
    return scores[1]

  return scores[0]
}

module.exports = {
  isScala,
  isScalaReale,
  isPoker,
  isFull,
  isTris,
  isDoppiaCoppia,
  isCoppia,
  getScore,
  apertura,
  getCoppiaVal,
  is45colore,
  getDoppiaCoppiaVal,
  getTrisVal,
  getFullVal,
  getScalaVal,
  getBestScore,
  getPokerChange,
  getTrisChange,
  getDoppiaCoppiaChange,
  get45coloreChange,
  getCoppiaChange
}
