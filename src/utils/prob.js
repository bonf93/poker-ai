var PD = require('probability-distributions')

module.exports = {
  oneOf: function (of, tot) {
    let a = Math.floor(Math.random() * tot) + 1
    if (a <= of)
      return true
    return false
  },

  normal: function (mean) {
    let d = -1
    // console.log('norm', Math.floor(Math.random() * Math.floor(mean / 100 * 30)) + Math.floor(mean / 100))
    while (d <= 0) {
      let deviation = Math.floor(Math.random() * Math.floor(mean / 100 * 30)) + Math.floor(mean / 100)
      let a = PD.rnorm(1000, mean, deviation)
      d = Math.round(a[Math.floor(Math.random() * a.length)])
    }

    return d
  }
}
